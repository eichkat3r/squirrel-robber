import numpy as np
import pygame

from .utils import load_image_from_pkgutil


class Particle:
    def __init__(self, position, direction, size, life, velocity):
        self.position = position
        self.direction = direction
        self.size = size
        self.life = life
        self.color = [np.random.randint(50, 200)] * 3
        self.velocity = velocity
        self.frame = -1

    def update(self):
        self.position = self.position + self.velocity * self.direction / 10
        if self.life > 0:
            self.life -= 1

    def blit(self, screen, surface):
        frames = surface.get_width() / surface.get_height()
        if self.frame == -1:
            self.frame = np.random.randint(0, frames)
        surface = pygame.transform.scale(surface, (self.size * frames, self.size))
        screen.blit(
            surface, self.position, (self.size * self.frame, 0, self.size, self.size)
        )


SMOKE = load_image_from_pkgutil("smoke.png")


class ParticleSpawner:
    def __init__(self, position, num_particles=50, life=-1):
        self.particles = []
        self.position = position
        self.life = life
        self.num_particles = num_particles
        self.surface = SMOKE

    def add_particle(self):
        position = np.copy(self.position)
        angle_rad = np.deg2rad(np.random.randint(0, 360))
        direction = np.array(
            [
                np.cos(angle_rad) - np.sin(angle_rad),
                np.sin(angle_rad) + np.cos(angle_rad),
            ]
        )
        direction += np.array([0, -1.0])
        size = np.random.randint(8, 32)
        life = np.random.randint(1, 100)
        velocity = np.random.randint(2, 5)
        particle = Particle(position, direction, size, life, velocity)
        self.particles.append(particle)

    def update(self):
        while len(self.particles) < self.num_particles and self.life != 0:
            self.add_particle()
        for particle in self.particles:
            particle.update()
        if self.life > 0:
            self.life -= 1
        self.particles = [p for p in self.particles if p.life > 0]

    def blit(self, screen):
        for particle in self.particles:
            particle.blit(screen, self.surface)
