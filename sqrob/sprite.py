import numpy as np
import pygame

from .utils import load_image_from_pkgutil


class Sprite:
    def __init__(self, image_path, scale=2):
        self.image_path = image_path
        self.surface = load_image_from_pkgutil(image_path)
        self._position = np.array([0, 0])
        self.clip_w = 32
        self.clip_h = 32
        self.scale(scale)
        self.x_frame = 0
        self.y_frame = 0
        self.moving = False

    def scale(self, factor):
        if factor == 2:
            self.surface = pygame.transform.scale2x(self.surface)
        else:
            self.surface = pygame.transform.scale(
                self.surface,
                (self.surface.get_width() * factor, self.surface.get_height() * factor),
            )
        self.clip_w *= factor
        self.clip_h *= factor

    def get_at(self, coordinate):
        return self.surface.get_at(
            (
                int(self.x_frame * self.clip_w + coordinate[0]),
                int(self.y_frame * self.clip_h + coordinate[1]),
            )
        )

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        self._position = value

    @property
    def x(self):
        return self.position[0]

    @property
    def y(self):
        return self.position[1]

    @x.setter
    def x(self, value):
        self.position = np.array([value, self.position[1]])

    @y.setter
    def y(self, value):
        self.position = np.array([self.position[0], value])

    def idle(self):
        self.x_frame = 0
        self.y_frame = 0

    def next_x_frame(self):
        self.x_frame = (self.x_frame + 1) % (self.surface.get_width() // self.clip_w)
        if not self.moving:
            self.idle()

    def next_y_frame(self):
        self.y_frame = (self.y_frame + 1) % (self.surface.get_height() // self.clip_h)
        if not self.moving:
            self.idle()

    def blit(self, screen, flip_x=False, flip_y=False):
        if flip_x or flip_y:
            surface = pygame.transform.flip(self.surface, flip_x, flip_y)
        else:
            surface = self.surface
        screen.blit(
            surface,
            (self.x, self.y),
            (
                self.clip_w * self.x_frame,
                self.clip_h * self.y_frame,
                self.clip_w,
                self.clip_h,
            ),
        )
        self.moving = False

    @property
    def bbox(self):
        return (self.x, self.y, self.clip_w, self.clip_h)

    def in_rect(self, rect):
        if self.x + self.clip_w < rect[0]:
            return False
        if self.y + self.clip_h < rect[1]:
            return False
        if self.x >= rect[0] + rect[2]:
            return False
        if self.y >= rect[1] + rect[3]:
            return False
        return True
