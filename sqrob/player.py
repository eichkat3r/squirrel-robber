import numpy as np

from .entity import Entity
from .playerprofile import player_profiles
from .utils import blit_rotated
from .weapon import weapons


class Player(Entity):
    def __init__(self):
        super().__init__()
        self.profile = player_profiles["eichkat3rina"]
        self.weapon = weapons["leafblower"]
        self._position = np.array([200, 200])
        self._weapon_angle = 0
        self._weapon_position = self._position + np.array([28 * 2, 20 * 2])
        self.frame_update = 0
        self.velocity = 2

    @property
    def width(self):
        return self.profile.width

    @property
    def height(self):
        return self.profile.height

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, value):
        if value[0] == -1 and self._direction[0] == 1:
            x = np.cos(np.deg2rad(self._weapon_angle))
            y = np.sin(np.deg2rad(self._weapon_angle))
            self._weapon_angle = np.rad2deg(np.arctan2(y, -x))
        if value[0] == 1 and self._direction[0] == -1:
            x = np.cos(np.deg2rad(self._weapon_angle))
            y = np.sin(np.deg2rad(self._weapon_angle))
            self._weapon_angle = np.rad2deg(np.arctan2(y, -x))
        self._direction = value

    @property
    def weapon_angle(self):
        return self._weapon_angle

    @weapon_angle.setter
    def weapon_angle(self, value):
        self._weapon_angle = value % 360

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, pos):
        self._position = pos
        if self._direction[0] == -1:
            self._weapon_position = self._position + np.array([4 * 2, 20 * 2])
        else:
            self._weapon_position = self._position + np.array([28 * 2, 20 * 2])

    @property
    def weapon_position(self):
        return self._weapon_position

    def move(self, vector):
        self.position = np.array(
            [self.position[0] + vector[0], self.position[1] + vector[1]]
        )

    @property
    def moving(self):
        return self.profile.moving

    @moving.setter
    def moving(self, value):
        self.profile.moving = value

    def blit(self, screen):
        if self.frame_update == 0:
            self.profile.next_x_frame()
        self.profile.blit(screen, self._position, flip_x=self.direction[0] < 0)
        blit_rotated(
            screen,
            self.weapon.image,
            (self.weapon_position[0], self.weapon_position[1]),
            (self.weapon.pivot[0], self.weapon.pivot[1]),
            self.weapon_angle,
        )
        self.frame_update = (self.frame_update + 1) % 10

    def up(self, screen):
        if self.y - self.velocity >= 0:
            self.y -= self.velocity
            self.weapon_angle = (self.weapon_angle + 2 * np.sin(self.y / 10)) % 360
            self.moving = True
            self.direction = [self.direction[0], -1]

    def down(self, screen):
        if self.y + self.velocity + self.width < screen.get_height():
            self.y += self.velocity
            self.weapon_angle = (self.weapon_angle + 2 * np.sin(self.y / 10)) % 360
            self.moving = True
            self.direction = [self.direction[0], 1]

    def left(self, screen):
        if self.x - self.velocity >= 0:
            self.x -= self.velocity
            self.weapon_angle = (self.weapon_angle + 2 * np.cos(self.x / 10)) % 360
            self.moving = True
            self.direction = [-1, self.direction[1]]

    def right(self, screen):
        if self.x + self.velocity + self.width < screen.get_width():
            self.x += self.velocity
            self.weapon_angle = (self.weapon_angle + 2 * np.cos(self.x / 10)) % 360
            self.moving = True
            self.direction = [1, self.direction[1]]

    def __in__(self, data):
        if data.shape == (2,):
            # point
            if data[0] >= self.bbox[0] + self.bbox[2]:
                return False
            if data[0] < self.bbox[0]:
                return False
            if data[1] >= self.bbox[1] + self.bbox[3]:
                return False
            if data[1] < self.bbox[1]:
                return False
            return True
        elif data.shape == (4,):
            # rect
            TL = np.array([data[0], data[1]])
            TR = np.array([data[0] + data[2], data[1]])
            BL = np.array([data[0], data[1] + data[3]])
            BR = np.array([data[0] + data[2], data[1] + data[3]])
            return TL in self and TR in self and BL in self and BR in self

    def intersect(self, data):
        TL = np.array([data[0], data[1]])
        TR = np.array([data[0] + data[2], data[1]])
        BL = np.array([data[0], data[1] + data[3]])
        BR = np.array([data[0] + data[2], data[1] + data[3]])
        return self.__in__(TL) or self.__in__(TR) or self.__in__(BL) or self.__in__(BR)

    @property
    def bbox(self):
        return (
            self.x + 5,
            self.y + self.height // 2,
            self.width - 10,
            self.height - 10,
        )
