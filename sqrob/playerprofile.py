from .sprite import Sprite


player_base = {"squirrel": Sprite("squirrel.png")}

player_hairstyles = {
    None: None,
    "pigtails": Sprite("pigtails.png"),
}

player_accessories = {
    None: None,
    "glasses": Sprite("glasses.png"),
}


class PlayerProfile:
    def __init__(self, name, base, hairstyle, accessories):
        self._name = name
        if base not in player_base:
            raise RuntimeError(f"Invalid player base {base}.")
        self._base = player_base[base]
        if hairstyle not in player_hairstyles:
            raise RuntimeError(f"Invalid player hairstyle {hairstyle}.")
        self._hairstyle = player_hairstyles[hairstyle]
        if accessories not in player_accessories:
            raise RuntimeError(f"Invalid player accessories {accessories}.")
        self._accessories = player_accessories[accessories]
        self._moving = False

    def blit(self, screen, position, flip_x=False):
        self._base.position = position
        self._base.blit(screen, flip_x=flip_x)
        if self._hairstyle:
            self._hairstyle.position = position
            self._hairstyle.blit(screen, flip_x=flip_x)
        if self._accessories:
            self._accessories.position = position
            self._accessories.blit(screen, flip_x=flip_x)

    def in_rect(self, rect):
        return self._base.in_rect(rect)

    @property
    def width(self):
        return self._base.clip_w

    @property
    def height(self):
        return self._base.clip_h

    def next_x_frame(self):
        self._base.next_x_frame()
        if self._hairstyle:
            self._hairstyle.next_x_frame()
        if self._accessories:
            self._accessories.next_x_frame()

    @property
    def moving(self):
        return self._moving

    @moving.setter
    def moving(self, value):
        self._moving = value
        self._base.moving = value
        if self._hairstyle:
            self._hairstyle.moving = value
        if self._accessories:
            self._accessories.moving = value


player_profiles = {
    "eichkat3r": PlayerProfile("eichkat3r", "squirrel", None, None),
    "eichkat3rina": PlayerProfile("eichkat3rina", "squirrel", "pigtails", "glasses"),
}
