import time

import pygame
import numpy as np

from .camera import Camera


class Application:
    def __init__(self, title="squirrel robber", dimensions=(800, 600)):
        np.random.seed(int(time.time()))
        pygame.init()
        pygame.joystick.init()
        pygame.display.set_caption(title)
        self.sounds_enabled = True
        self._screen = pygame.display.set_mode(dimensions)
        self._running = False
        self.clock = pygame.time.Clock()
        self._joysticks = [
            pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())
        ]
        self._views = []
        self.camera = Camera(self._screen)

    def push_view(self, v):
        self._views.append(v)

    def pop_view(self):
        self._views.pop()
        if not self._views:
            self._running = False

    @property
    def joysticks(self):
        return self._joysticks

    @property
    def screen(self):
        return self._screen

    def run(self):
        self._running = True
        while self._running:
            self.screen.fill((0, 0, 0))
            self._views[-1].run(self)
            pygame.display.update()
            self.clock.tick(60)

    def __del__(self):
        pygame.quit()


class View:
    def __init__(self):
        pass

    def run(self, app):
        pass
