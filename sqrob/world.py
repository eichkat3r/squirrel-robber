import numpy as np

from .room import Room


class World:
    def __init__(self, generations=5):
        self._rooms = []
        self._current_room_index = 0
        self._map = None
        self._generations = generations
        self._generate()

    def _generate(self):
        self._rooms.clear()
        self._current_room_index = 0
        self._rooms.append(Room())

    @property
    def current_room(self):
        if not self._rooms:
            raise RuntimeError(
                "No rooms in world. Call generate() first. This is a bug."
            )
        return self._rooms[self._current_room_index]
