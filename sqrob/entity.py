import numpy as np


class Entity:
    def __init__(self):
        self._position = np.array([0, 0])
        self._direction = np.array([1, 1])
        self._health = 5

    @property
    def alive(self):
        return self.health > 1e-3

    @property
    def health(self):
        return self._health

    @health.setter
    def health(self, value):
        self._health = np.clip(value, 0, np.inf)

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, value):
        self._direction = value

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        self._position = value

    @property
    def x(self):
        return self._position[0]

    @property
    def y(self):
        return self._position[1]

    @x.setter
    def x(self, value):
        # set whole array to update property
        self.position = np.array([value, self.position[1]])

    @y.setter
    def y(self, value):
        # set whole array to update property
        self.position = np.array([self.position[0], value])

    @property
    def bbox(self):
        return (self.x + 5, self.y + 5, self.width - 10, self.height - 10)

    def __in__(self, data):
        if data.shape == (2,):
            # point
            if data[0] >= self.bbox[0] + self.bbox[2]:
                return False
            if data[0] < self.bbox[0]:
                return False
            if data[1] >= self.bbox[1] + self.bbox[3]:
                return False
            if data[1] < self.bbox[1]:
                return False
            return True
        elif data.shape == (4,):
            # rect
            return (
                self.__in__(data[0])
                and self.__in__(data[0] + data[2])
                and self.__in__(data[1])
                and self.__in__(data[1] + data[3])
            )

    def intersect(self, data):
        return (
            self.__in__(data[0])
            and self.__in__(data[0] + data[2])
            and self.__in__(data[1])
            and self.__in__(data[1] + data[3])
        )
