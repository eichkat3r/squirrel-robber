#!/usr/bin/env python3
from setuptools import setup
import pathlib

here = pathlib.Path(__file__).parent.resolve()

long_description = (here / "README.md").read_text(encoding="utf-8")

setup(
    name="sqrob",
    version="0.1.0",
    description="",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="eichkat3r",
    author_email="mail@katerbase.de",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3 :: Only",
    ],
    keywords="squirrel, game",
    packages=["assets", "assets.images"],
    python_requires=">=3.7, <4",
    install_requires=["pygame", "click", "numpy"],
    package_dir={
        "assets": "assets",
        "assets.images": "assets/images",
        "assets.sounds": "assets/sounds",
    },
    include_package_data=True,
    package_data={
        "assets": ["assets"],
        "assets.images": ["assets/images/*.png"],
        "assets.sounds": [
            "assets/sounds/*.wav",
            "assets/sounds/*.mp3",
        ],
    },
    data_files=[
        ("/usr/share/applications", ["robots_vs_squirrels.desktop"]),
        ("/usr/share/icons", ["robots_vs_squirrels.png"]),
    ],
    entry_points="""
        [console_scripts]
        robotsvssquirrels = sqrob:main
    """,
)
