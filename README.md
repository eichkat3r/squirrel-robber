# squirrel-robber

Work-in-progress game about a squirrel that has to save the world from evil robo-squirrels.

![](static/teaser.gif)

## Preparations 
### Virtual Python environments

```bash
sudo apt-get install python3-venv -y 
```

```bash
python3 -m venv 
```

```bash
source venv/bin/activate
```

With linux-run.sh all dependencies are installed and the game is started.

## Installation

### Via setup.py

Install the project via:

```bash
sudo python3 ./setup.py install
```

and run

```bash
squirrel-robber
```

to start the game.